This project is meant for Spark batch and real-time assessment.

* Create a Spark application that reads the CSV and loads the data into a Hbase (Bigtable in GCP) table.
    * Spark application need to be written in Scala, with unit test cases.
    * csv to be processed is present in the /data/week1
        * csv data contains the sample cycle ride data from Strava application for 3 customers.
        * this csv data need to be loaded in Hbase table called cycleride 
        * Hbase table need to be designed in a way that it can be scanned per cust_id and it will show all the records for that customer.

* For the environment setup read the environment_guide. 

### Submitting assessment
* Create a 	__**private project**__ in Gitlab, upload the code in the project and along with the jar
* The code repo should also contains the hbase scripts too.